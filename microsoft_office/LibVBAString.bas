Attribute VB_Name = "LibVBAString"
Option Base 0
Option Compare Text
Option Explicit

Function AlphaNumerics(ByVal TheString As String, Optional LowerCase As Boolean = True, Optional StripPercentEncoding As Boolean) As String
  LoggerIndent "AlphaNumerics"

  Dim TheLength As Long, ThePosition As Long, TheChar As String
  Dim Alnum As String

  Alnum = TheString

  If LowerCase Then Alnum = LCase(Alnum)

  If StripPercentEncoding Then Alnum = StripURLEncoding(Alnum)

  Alnum = RegExpReplace(ThePattern:="[^0-9A-Z]", TheString:=Alnum, IgnoreCase:=True)

  AlphaNumerics = Alnum

  LoggerDebugLib TheString & " -> " & Alnum
  LoggerDedent
End Function

Function EndsWith(TheString As String, TheSuffix As String) As Boolean
  LoggerIndent "EndsWith"

  Dim SuffixLength As Integer

  If TheSuffix = "" Then GoTo ExitProcedure

  SuffixLength = Len(TheSuffix)

  If Right(TheString, SuffixLength) = TheSuffix Then EndsWith = True

ExitProcedure:
  LoggerDedent
End Function

Function RegExpReplace( _
  ThePattern As String, TheString As String, Optional ReplaceWith As String, _
  Optional IgnoreCase As Boolean, Optional ReplaceGlobally As Boolean = True _
) As String

  LoggerIndent "RegExpReplace"

  Dim RegEx As Object
  Dim ReplacedString As String

  Set RegEx = CreateObject("VBScript.RegExp")
  RegEx.Pattern = ThePattern
  RegEx.IgnoreCase = IgnoreCase
  RegEx.Global = ReplaceGlobally

  ReplacedString = RegEx.Replace(TheString, ReplaceWith)

  RegExpReplace = ReplacedString

  LoggerDebugLib TheString & " -> " & ReplacedString
  LoggerDedent
End Function

Function StartsWith(TheString As String, ThePrefix As String) As Boolean
  LoggerIndent "StartsWith"

  Dim PrefixLength As Integer

  If ThePrefix = "" Then GoTo ExitProcedure

  PrefixLength = Len(ThePrefix)

  If Left(TheString, PrefixLength) = ThePrefix Then StartsWith = True

ExitProcedure:
  LoggerDedent
End Function

Function StringRepeat(TheString As String, RepeatCount As Byte) As String
  LoggerIndent "StringRepeat"

  Dim TheCount As Byte, TheRepeatedString As String

  For TheCount = 0 To RepeatCount
    TheRepeatedString = TheRepeatedString & TheString
  Next

  StringRepeat = TheRepeatedString

  LoggerDebugLib TheRepeatedString
  LoggerDedent
End Function

Function StripURLEncoding(TheString As String) As String
  LoggerIndent "StripURLEncoding"

  Dim StrippedString As String

  StrippedString = RegExpReplace( _
    ThePattern:="%[0-9A-F]{2}", TheString:=TheString, _
    IgnoreCase:=True, ReplaceGlobally:=True)

  StripURLEncoding = StrippedString

  LoggerDebugLib TheString & " -> " & StrippedString
  LoggerDedent
End Function

Function SplitChars(TheString As String)
  LoggerIndent "SplitChars"

  Dim TheLength As Long, TheIndex As Long, TheChar As String
  Dim TheChars

  TheLength = Len(TheString)
  ReDim TheChars(TheLength - 1)

  For TheIndex = 0 To TheLength - 1
    TheChars(TheIndex) = Mid(TheString, TheIndex + 1, 1)
  Next

  SplitChars = TheChars

  LoggerDebugLib TheChars
  LoggerDedent
End Function

