VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} MailReminder 
   Caption         =   "Due Date"
   ClientHeight    =   4140
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   3855
   OleObjectBlob   =   "MailReminder.frx":0000
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "MailReminder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Base 0
Option Compare Text
Option Explicit

Private Sub UserForm_Initialize()
  Me.txtMessage = "Format: YYYY-MM-DD or MM-DD or DD"
End Sub

Private Sub txtDate_Enter()
  Dim text As String
  
  text = Me.txtDate.Value
  If text = "" Then
    Me.txtDate.Value = Format(Now() + 8, "YYYY-MM-DD")
  End If
End Sub

Private Sub txtDate_AfterUpdate()
  Dim text As String, text_array() As String, size As Integer
  Dim due_day As Integer, due_month As Integer, due_year As Integer, due_date As Date
  
  text = Me.txtDate.Value
  If text = "" Then GoTo exit_sub
  
  text_array = Split(text, "-")
  size = UBound(text_array)
  
  'On Error GoTo err_invalid_date
  If size > 2 Then
    GoTo err_invalid_date
  ElseIf size = 2 Then
    due_year = CInt(text_array(0))
    due_month = CInt(text_array(1))
    due_day = CInt(text_array(2))
  ElseIf size = 1 Then
    due_year = year(Date)
    due_month = CInt(text_array(0))
    due_day = CInt(text_array(1))
  Else
    due_year = year(Date)
    due_month = month(Date)
    due_day = CInt(text_array(0))
  End If
  
  due_date = DateSerial(due_year, due_month, due_day)
  SetDate due_date, due_date
  GoTo exit_sub

err_invalid_date:
  Me.txtMessage = "Invalid date. Format: [[YYYY-]MM-]DD"
exit_sub:
End Sub

Private Sub cmdNoDate_Click()
  SetMarkInterval olMarkNoDate
End Sub

Private Sub cmdToday_Click()
  SetMarkInterval olMarkToday
End Sub

Private Sub cmdTomorrow_Click()
  SetMarkInterval olMarkTomorrow
End Sub

Private Sub cmdThisWeek_Click()
  SetMarkInterval olMarkThisWeek
End Sub

Private Sub cmdNextWeek_Click()
  SetMarkInterval olMarkNextWeek
End Sub

Private Sub cmdThisMonth_Click()
  Dim first_day As Date, last_day As Date
  
  ' first_day = tomorrow
  first_day = DateAdd(Interval:="d", Number:=1, Date:=Date)
  last_day = DateSerial(year(Date), month(Date) + 1, 0)
  
  SetDate first_day, last_day
End Sub

Private Sub cmdNextMonth_Click()
  Dim first_day As Date, last_day As Date
  
  first_day = DateSerial(year(Date), month(Date) + 1, 1)
  last_day = DateSerial(year(Date), month(Date) + 2, 0)
  
  SetDate first_day, last_day
End Sub

Private Sub cmdClear_Click()
  ClearTaskFlag
End Sub

Private Sub cmdComplete_Click()
  SetMarkInterval olMarkComplete
End Sub

Private Sub cmdMon_Click()
  SetDayOfWeek vbMonday
End Sub

Private Sub cmdTue_Click()
  SetDayOfWeek vbTuesday
End Sub

Private Sub cmdWed_Click()
  SetDayOfWeek vbWednesday
End Sub

Private Sub cmdThu_Click()
  SetDayOfWeek vbThursday
End Sub

Private Sub cmdFri_Click()
  SetDayOfWeek vbFriday
End Sub

Private Sub ClearTaskFlag()
  Dim oItems As Outlook.Selection
  Dim oItem As Outlook.MailItem
  
  Set oItems = Application.ActiveExplorer.Selection
  For Each oItem In oItems
    DebugPrint "<", oItem
    
    If oItem.IsMarkedAsTask Then
      oItem.MarkAsTask olMarkNoDate
    End If
    oItem.ClearTaskFlag
    
    SaveItem oItem
  Next
  
  Debug.Print
  Me.txtMessage = "Task Flag cleared."
End Sub

Private Sub SetDayOfWeek(target_dow As Integer)
  Dim current_dow As Integer, due_date As Date
  
  current_dow = Weekday(Date:=Date, FirstDayOfWeek:=vbSunday)
  
  If current_dow < target_dow Then
    due_date = DateAdd(Interval:="d", Number:=target_dow - current_dow, Date:=Date)
  Else
    due_date = DateAdd(Interval:="d", Number:=7 + (target_dow - current_dow), Date:=Date)
  End If
  
  SetDate due_date, due_date
End Sub

Private Sub SetDate(start_date As Date, due_date As Date)
  Dim oItems As Outlook.Selection
  Dim oItem As Outlook.MailItem
  
  Set oItems = Application.ActiveExplorer.Selection
  For Each oItem In oItems
    DebugPrint "<", oItem
    
    oItem.MarkAsTask olMarkNextWeek
    oItem.TaskStartDate = start_date
    oItem.TaskDueDate = due_date
    
    SaveItem oItem
  Next
  
  Debug.Print
  Me.txtMessage = "Flag set: Start=" & start_date & " Due=" & due_date
End Sub

Private Sub SetMarkInterval(oMarkInterval)
  Dim oItems As Outlook.Selection
  Dim oItem As Outlook.MailItem
  Dim fUnread As Boolean
  
  Set oItems = Application.ActiveExplorer.Selection
  For Each oItem In oItems
    DebugPrint "<", oItem
    
    If oMarkInterval = olMarkComplete Then
      OnlyMarkAsCompleteIfNotComplete oItem
    Else
      oItem.MarkAsTask oMarkInterval
    End If
  
    If oMarkInterval = olMarkToday Then fUnread = True
    SaveItem oItem, fUnread
  Next
  
  Debug.Print
  Me.txtMessage = "Operation successful"
End Sub

Private Sub OnlyMarkAsCompleteIfNotComplete(oItem As Outlook.MailItem)
  If oItem.TaskCompletedDate = "4501-01-01" Then
    oItem.MarkAsTask olTaskComplete
    ' oItem.MarkAsTask olTaskComplete doesn't set TaskCompletionDate
    oItem.TaskCompletedDate = Date
  End If
End Sub

Private Sub DebugPrint(direction As String, oItem As Outlook.MailItem)
  Debug.Print direction & " " & oItem.IsMarkedAsTask, oItem.TaskStartDate, oItem.TaskDueDate
End Sub

Private Sub SaveItem(oItem As Outlook.MailItem, Optional fUnread As Boolean = False)
    DebugPrint ">", oItem
    oItem.Unread = fUnread
    oItem.Save
End Sub
