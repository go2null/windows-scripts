Attribute VB_Name = "LibOutlookFolder"
Option Base 0
Option Compare Text
Option Explicit

Public Sub IterateItemsInFolder()
  LoggerIndent "DeleteEmptyFolder"

  Dim OutlookFolder As Outlook.Folder, FolderItem As Object

  Set OutlookFolder = Application.ActiveExplorer.CurrentFolder
  For Each FolderItem In OutlookFolder.Items
    With FolderItem
      Debug.Print .Class & " | " & .MessageClass & " | " & .Subject
      If .MessageClass = "IPM.Note" Then Debug.Assert False
    End With
  Next

  LoggerDedent
End Sub

Function DeleteEmptyFolder(OutlookFolder As Outlook.Folder) As Boolean
  LoggerIndent "DeleteEmptyFolder"
  Dim ErrorNumber As Long
  On Error GoTo ErrorHandler

  If IsRootFolder(OutlookFolder) Then GoTo UndeletableFolder
  If IsDefaultFolder(OutlookFolder) Then GoTo UndeletableFolder

  If OutlookFolder.Folders.Count > 0 Then GoTo NotEmptyFolder
  If OutlookFolder.Items.Count > 0 Then GoTo NotEmptyFolder

  OutlookFolder.Delete
  LoggerInfo "DELETED " & OutlookFolder.FolderPath
  DeleteEmptyFolder = True

  GoTo ExitProcedure

UndeletableFolder:
  LoggerDebugLib "Cannot delete folder " & OutlookFolder.FolderPath
  GoTo ExitProcedure

NotEmptyFolder:
  LoggerDebugLib "Folder is not empty " & OutlookFolder.FolderPath
  GoTo ExitProcedure

ErrorHandler:
  ErrorNumber = Err.Number
  If MatchError(ErrorDescription:=Err.Description, MatchDescription:="Cannot delete this folder.") Then
    GoTo UndeletableFolder
  Else
    Err.Raise ErrorNumber
  End If

ExitProcedure:
  LoggerDedent
End Function

' Recursive search starting from StartFolder,
' optionally looking for specific path if ParentPath provided.
' RETURNS the first folder found with FolderName
Function FindFolder(FolderName As String, StartFolder As Object, Optional ParentPath As String) As Outlook.Folder
  LoggerIndent "FindFolder"

  Dim AlnumName As String, AlnumPath As String, AlnumPathLength As Integer
  Dim OutlookFolder As Outlook.Folder, FoundFolder As Outlook.Folder

  AlnumName = AlphaNumerics(FolderName)
  AlnumPath = AlphaNumerics(TheString:=ParentPath & FolderName, StripPercentEncoding:=True)
  AlnumPathLength = Len(AlnumPath)

  For Each OutlookFolder In StartFolder.Folders
    If AlphaNumerics(OutlookFolder.Name) = AlnumName _
    And Right(AlphaNumerics(OutlookFolder.FolderPath, StripPercentEncoding:=True), AlnumPathLength) = AlnumPath _
    Then
      Set FoundFolder = OutlookFolder
      Exit For
    End If

    ' recurse
    If OutlookFolder.Folders.Count = 0 Then GoTo ContinueFor

    Set FoundFolder = FindFolder(AlnumName, OutlookFolder, ParentPath)
    If Not FoundFolder Is Nothing Then Exit For
ContinueFor:
  Next

  If FoundFolder Is Nothing Then
    LoggerDebugLib FolderName & " not found in " & StartFolder.FolderPath & "\"
  Else
    LoggerDebug FolderName & " found at " & FoundFolder.FolderPath
    Set FindFolder = FoundFolder
  End If

  LoggerDedent
End Function

Function GetChildFolder(ParentFolder As Outlook.Folder, ChildFolderName As String) As Outlook.Folder
  LoggerIndent "GetChildFolder"

  Dim FolderName As String
  Dim OutlookFolder As Outlook.Folder, ChildFolder As Outlook.Folder

  FolderName = AlphaNumerics(ChildFolderName)

  For Each OutlookFolder In ParentFolder.Folders
    If AlphaNumerics(OutlookFolder.Name) = FolderName Then
      Set ChildFolder = OutlookFolder
      Exit For
    End If
  Next

  If Not ChildFolder Is Nothing Then Set GetChildFolder = ChildFolder

  LoggerDedent
End Function

Function IsMailFolder(OutlookFolder As Outlook.Folder) As Boolean
  LoggerIndent "IsMailFolder"

  ' DefaultMessageClass : "IPM.Note" : String
  If OutlookFolder.DefaultItemType = olMailItem Then IsMailFolder = True

  LoggerDedent
End Function

Function MoveFolder(FromFolder As Outlook.Folder, ToParentFolder As Outlook.Folder) As Boolean
  LoggerIndent "MoveFolder"

  Dim ToFolder As Outlook.Folder
  Dim SubFolder As Outlook.Folder

  Set ToFolder = GetChildFolder(ToParentFolder, FromFolder.Name)

  If ToFolder Is Nothing Then
    LoggerInfo FromFolder.FolderPath & " -> " & ToParentFolder.FolderPath & "/"
    FromFolder.MoveTo ToParentFolder
  Else
    ' Recursively move existing subfolders
    For Each SubFolder In FromFolder.Folders
      MoveFolder SubFolder, ToFolder
    Next

    LoggerDebug FromFolder.FolderPath & " -> " & ToFolder.FolderPath
    If MoveFolderItems(FromFolder, ToFolder) Then
      DeleteEmptyFolder FromFolder
    End If
  End If

  MoveFolder = True

  LoggerDedent True
End Function

Function MoveFolderItems(FromFolder As Outlook.Folder, ToFolder As Outlook.Folder) As Boolean
  LoggerIndent "MoveFolderItems"

  Dim OutlookItem As Object

  For Each OutlookItem In FromFolder.Items
    LoggerInfo OutlookItem.Subject
    If Not IsSignedItem(OutlookItem) Then
      OutlookItem.Move ToFolder
    End If
  Next

  If FromFolder.Items.Count = 0 Then MoveFolderItems = True

  LoggerDedent True
End Function

Function SanitizeFolderName(OutlookFolder As Outlook.Folder) As Boolean
  LoggerIndent "RenameFolder"
  Dim ErrorNumber As Long
  On Error GoTo ErrorHandler

  Dim OldName As String, NewName As String

  If IsRootFolder(OutlookFolder) Then GoTo ExitProcedure

  OldName = OutlookFolder.Name
  If ArrayIncludes(GetDefaultFolders, OutlookFolder) Then
    GoTo ExitProcedure
  ElseIf ArrayIncludes(GetDefaultFolderNames, OldName) Then
    GoTo ExitProcedure
  End If

  NewName = Replace(OutlookFolder.Name, " ", "_")
  If NewName = OutlookFolder.Name Then GoTo NoChange

  OutlookFolder.Name = NewName

  LoggerInfo OutlookFolder.FolderPath & " <- " & OldName
  SanitizeFolderName = True

  GoTo ExitProcedure

NoChange:
  LoggerDebugLib OutlookFolder.Name & "is already sanitary"
  GoTo ExitProcedure

ErrorHandler:
  ErrorNumber = Err.Number
  ' Various ErrNumber, but text always "You don't have appropriate permission to perform this operation."
  If MatchError(Err.Number, Err.Description, _
    MatchDescription:="You don't have appropriate permission to perform this operation") _
  Then
    Resume Next
  Else
    Err.Raise ErrorNumber
    Resume
  End If

ExitProcedure:
  LoggerDedent
End Function

Function SetShowUnreadItemCount(OutlookFolder As Outlook.Folder) As Boolean
  LoggerIndent "SetShowUnreadItemCount"
  Dim ErrorNumber As Long
  On Error GoTo ErrorHandler

  If OutlookFolder.ShowItemCount = olShowUnreadItemCount Then GoTo ExitProcedure

  OutlookFolder.ShowItemCount = olShowUnreadItemCount

  LoggerInfo "SET Show Unread Item Count " & OutlookFolder.FolderPath
  SetShowUnreadItemCount = True

  GoTo ExitProcedure

ErrorHandler:
  ErrorNumber = Err.Number
  If MatchError(Err.Number, Err.Description) Then
    Resume Next
  Else
    Err.Raise ErrorNumber
  End If

ExitProcedure:
  LoggerDedent
End Function
