Attribute VB_Name = "FolderConversation"
Option Base 0
Option Compare Text
Option Explicit

Function Conversation_MoveToThreadFolder(FolderItem As Object) As Boolean
  LoggerIndent "Conversation_MoveToThreadFolder"

  On Error GoTo ErrorHandler
  Dim ErrorNumber As Long, ErrorDescription As String

  Dim OutlookConversation As Outlook.Conversation
  Dim ParentItem As Object

  If FolderItem.Class <> OlObjectClass.olMail Then GoTo ExitProcedure

  'skip signed messages as they do not allow access to attributes and methods
  If Not IsAutomatableItem(FolderItem) Then GoTo ExitProcedure

  If Not IsDefaultFolder(FolderItem.Parent) Then GoTo ExitProcedure

  Set OutlookConversation = FolderItem.GetConversation
  If OutlookConversation Is Nothing Then GoTo ExitProcedure

  Set ParentItem = OutlookConversation.GetParent(FolderItem)

  If ParentItem Is Nothing Then GoTo ExitProcedure
  If IsDefaultFolder(ParentItem.Parent) Then GoTo ExitProcedure

  LoggerInfo FolderItem.Parent.FolderPath & "  -> " & ParentItem.Parent.FolderPath
  FolderItem.Move ParentItem.Parent

  Conversation_MoveToThreadFolder = True

  GoTo ExitProcedure

ErrorHandler:
  ErrorNumber = Err.Number
  ErrorDescription = Err.Description
  If MatchError(ErrorDescription:=ErrorDescription, MatchDescription:="The item passed to the method does not exist in this conversation.") Then
    Resume Next
  ElseIf MatchError(ErrorDescription:=ErrorDescription, MatchDescription:="A timeout occurred when obtaining conversation results.") Then
    Resume Next
  Else
    Err.Raise ErrorNumber
    Resume
  End If

ExitProcedure:
  LoggerDedent
End Function

Sub MoveToConversationFolder(Optional OutlookFolder As Outlook.Folder)
  LoggerIndent "MoveToConversationFolder"

  Dim TimerStart As Single, TimerStartEach As Single, TimerStartEachRoot As Single
  Dim OutlookItems As Object, OutlookItem As Object
  Dim OutlookConversation As Outlook.Conversation
  Dim RootItems As Outlook.SimpleItems, RootItem
  Dim ResetToFolder As Boolean

  TimerStart = Timer

  If OutlookFolder Is Nothing Then
    Set OutlookItems = Application.ActiveExplorer.Selection
  Else
    If IsInboxFolder(OutlookFolder) Or IsSentItemsFolder(OutlookFolder) Then
      Set OutlookItems = OutlookFolder.items
    Else
      GoTo ExitProcedure
    End If
  End If
  If OutlookItems.Count = 0 Then GoTo ExitProcedure

  For Each OutlookItem In OutlookItems
    TimerStartEach = Timer

    If OutlookItem.Class <> OlObjectClass.olMail Then GoTo ContinueFor
    If IsDefaultFolder(OutlookFolder) Then GoTo ExitProcedure

    Set OutlookConversation = OutlookItem.GetConversation
    If OutlookConversation Is Nothing Then GoTo ContinueFor

    ResetToFolder = True
    Set RootItems = OutlookConversation.GetRootItems
    For Each RootItem In RootItems
      TimerStartEachRoot = Timer

      MoveItemsToFolder OutlookConversation, RootItem, ResetToFolder
      ResetToFolder = False

      LoggerDebug (Timer - TimerStartEachRoot) & " " & "MoveToConversationFolder::OutlookItem::RootItem"
    Next
ContinueFor:
    LoggerDebug (Timer - TimerStartEach) & " " & "MoveToConversationFolder::OutlookItem"
  Next

ExitProcedure:
  LoggerDebug (Timer - TimerStart) & " seconds"
  LoggerDedent
End Sub

Private Sub MoveItemsToFolder(OutlookConversation As Outlook.Conversation, ConversationNode, Optional ByVal ResetToFolder As Boolean = False)
  LoggerIndent "MoveItemsToFolder"

  Static RecursionLevel As Byte
  Static ToFolder As Outlook.Folder

  Dim TimerStart As Single
  Dim OutlookMailItem As Outlook.MailItem
  Dim ItemFolder As Outlook.Folder

  TimerStart = Timer

  If ResetToFolder Then
    RecursionLevel = 2
    Set ToFolder = Nothing
  Else
    RecursionLevel = RecursionLevel + 1
  End If

  If ConversationNode.Class = OlObjectClass.olMail Then
    Set OutlookMailItem = ConversationNode

    Set ItemFolder = OutlookMailItem.Parent
    Select Case FolderType(ItemFolder)
      Case "move_from"
        If Not ToFolder Is Nothing Then OutlookMailItem.Move ToFolder
      Case "ignore"
      Case "change_to"
        Set ToFolder = ItemFolder
    End Select
  End If

  RecurseChildren OutlookConversation, ConversationNode

ExitProcedure:
  LoggerDebug Timer - TimerStart & " " & RecursionLevel
  LoggerDedent
End Sub

Private Sub RecurseChildren(OutlookConversation As Outlook.Conversation, ConversationNode)
  LoggerIndent "RecurseChildren"

  Dim ChildItems As Outlook.SimpleItems, ChildItem

  ' don't know why this errors for some nodes
  On Error Resume Next
    Set ChildItems = OutlookConversation.GetChildren(ConversationNode)
  On Error GoTo 0
  If ChildItems Is Nothing Then GoTo ExitProcedure

  For Each ChildItem In ChildItems
    MoveItemsToFolder OutlookConversation, ChildItem
  Next

ExitProcedure:
  LoggerDedent
End Sub

Private Function FolderType(ByVal OutlookFolder As Outlook.Folder) As String
  LoggerIndent "FolderType"

  Dim DefaultFolder As Outlook.Folder

  If StartsWith(OutlookFolder.Name, ".") Then
    FolderType = "ignore"
  ElseIf IsInboxFolder(OutlookFolder) Or IsSentItemsFolder(OutlookFolder) Then
    FolderType = "move_from"
  ElseIf IsDefaultFolder(OutlookFolder) Then
    FolderType = "ignore"
  Else
    FolderType = "change_to"
  End If

  LoggerDedent
End Function

