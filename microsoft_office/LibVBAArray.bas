Attribute VB_Name = "LibVBAArray"
Option Base 0
Option Compare Text
Option Explicit

Function ArrayIsEmpty(TheArray) As Boolean
  LoggerIndent "ArrayIsEmpty"

  Dim ArraySize As Long, IsEmpty As Boolean

  On Error Resume Next
    ArraySize = UBound(TheArray)
    If Err.Number <> 0 Then IsEmpty = True
  On Error GoTo 0

  LoggerDebugLib IsEmpty
  ArrayIsEmpty = IsEmpty

  LoggerDedent
End Function

Function ArrayIncludes(TheArray, TheElement) As Boolean
  LoggerIndent "ArrayIncludes"

  Dim TheIndex As Long, IsFound As Boolean

  If ArrayIsEmpty(TheArray) Then GoTo ExitProcedure

  For TheIndex = LBound(TheArray) To UBound(TheArray)
    If TheArray(TheIndex) = TheElement Then
      IsFound = True
      Exit For
    End If
  Next

  LoggerDebugLib IsFound
  ArrayIncludes = IsFound

ExitProcedure:
  LoggerDedent
End Function

' Appends to an array
Function ArrayPush(TheArray, TheValue) As Boolean
  LoggerIndent "ArrayPush"

  Dim ArraySize As Long, NewIndex As Long, IsPushed As Boolean

  If ArrayIsEmpty(TheArray) Then
    NewIndex = 0
    ReDim TheArray(NewIndex)
  Else
    NewIndex = UBound(TheArray) + 1
    ReDim Preserve TheArray(NewIndex)
  End If

  If IsObject(TheValue) Then
    Set TheArray(NewIndex) = TheValue
  Else
    TheArray(NewIndex) = TheValue
  End If

  LoggerDebugLib IsPushed
  ArrayPush = True

  LoggerDedent
End Function

' Removes last entry from an array and returns it
Function ArrayPop(TheArray)
  LoggerIndent "ArrayPop"

  Dim ArraySize As Long, LogMessage As String

  If ArrayIsEmpty(TheArray) Then GoTo ExitProcedure

  ArraySize = UBound(TheArray)

  If IsObject(TheArray(ArraySize)) Then
    Set ArrayPop = TheArray(ArraySize)
  Else
    ArrayPop = TheArray(ArraySize)
  End If
  ReDim Preserve TheArray(ArraySize - 1)

  LogMessage = CStr(TheArray(ArraySize))
  LoggerDebugLib LogMessage

ExitProcedure:
  LoggerDedent
End Function
