Attribute VB_Name = "StoreRulesRunner"
Option Base 0
Option Compare Text
Option Explicit

Sub RunRulesOnAllMailboxes(Optional DontDisplayMessage As Boolean = False)
  LoggerIndent "RunRulesOnAllMailboxes"

  Dim StartTimer As Single
  StartTimer = Timer
  
  Dim PrimaryMailbox As Outlook.Store
  Dim MailboxRules, MailboxRule As Outlook.Rule, Index As Long
  Dim DefaultFolders(), DefaultFolder, OutlookFolder As Outlook.Folder
  Dim MessageFolders As String, MessageRules As String
  
  Set PrimaryMailbox = GetPrimaryMailbox
  MailboxRules = GetPrimaryMailboxRules
  DefaultFolders = GetDefaultFolders

  For Each DefaultFolder In DefaultFolders
    Set OutlookFolder = DefaultFolder

    If IsInboxFolder(OutlookFolder) _
      Or IsSentItemsFolder(OutlookFolder) _
    Then
      LoggerInfo OutlookFolder.Name & " " & OutlookFolder.FolderPath
      
      ' For Each only returns the first 5 rules in Microsoft 365
      For Index = 1 To UBound(MailboxRules)
        Set MailboxRule = MailboxRules(Index)
        
        If MailboxRule.Enabled _
          And IsAutomatedRunAllowed(MailboxRule) _
        Then
          LoggerInfo MailboxRule.Name
          MailboxRule.Execute _
            ShowProgress:=False, _
            Folder:=DefaultFolder, _
            IncludeSubfolders:=True
        Else
          LoggerDebugLib "Disabled " & MailboxRule.Name
        End If
      Next
    Else
      LoggerDebugLib "SKIP RULES ON " & OutlookFolder.Name & " in " & OutlookFolder.FolderPath
    End If
  Next
  
  If DontDisplayMessage Then
    Beep
  Else
    MsgBox "DONE in " & StoMS(Timer - StartTimer), vbOKOnly, "RunRulesOnAllMailboxes"
  End If
  
  LoggerDedent
End Sub

Private Function IsAutomatedRunAllowed(MailboxRule As Outlook.Rule) As Boolean
  If Left(MailboxRule.Name, 1) = ">" Then
    IsAutomatedRunAllowed = True
  Else
    IsAutomatedRunAllowed = False
  End If
End Function
