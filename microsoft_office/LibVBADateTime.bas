Attribute VB_Name = "LibVBADateTime"
Option Base 0
Option Compare Text
Option Explicit

Function StoMS(TheSeconds As Single) As String
  LoggerIndent "StoMS"
  Dim ErrorNumber As Long
  On Error GoTo ErrorHandler

  Dim ReturnValue As String
  Dim TheMinutes As Integer, TheRemainder As Single

  TheMinutes = Fix(TheSeconds / 60)
  TheRemainder = TheSeconds - (TheMinutes * 60)

  ReturnValue = Format(TheMinutes, "00") & ":" & Format(TheRemainder, "00.0")

  LoggerDebugLib ReturnValue
  StoMS = ReturnValue

  GoTo ExitProcedure

ErrorHandler:
  ErrorNumber = Err.Number
  If MatchError(Err.Number, Err.Description) Then
    Resume Next
  Else
    Err.Raise ErrorNumber
  End If

ExitProcedure:
  LoggerDedent
End Function

Function TimerMessage(TheMessage As String, MessageToAdd As String, StartTimer As Single) As String
  LoggerIndent "TimerMessage"

  TimerMessage = TheMessage & Chr(10) & StoMS(Timer - StartTimer) & " " & MessageToAdd

  LoggerDedent
End Function
