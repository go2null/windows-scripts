Attribute VB_Name = "LibOutlookMeeting"
Option Base 0
Option Compare Text
Option Explicit

' Reduce Start or End Time by TimeAdjustment minutes if they are at the top or bottom of the hour.
Function MeetingAdjustTime(MeetingDateTime As Date, TimeAdjustment As Integer) As Date
  LoggerIndent "MeetingAdjustTime"

  Dim MinuteOfTheHour As Byte, NewDateTime As Date

  MinuteOfTheHour = Minute(MeetingDateTime)

  If MinuteOfTheHour = 0 Or MinuteOfTheHour = 30 Then
    NewDateTime = DateAdd("n", TimeAdjustment, MeetingDateTime)
  Else
    NewDateTime = MeetingDateTime
  End If

  LoggerDebugLib MeetingDateTime & " -> " & NewDateTime
  MeetingAdjustTime = NewDateTime

  LoggerDedent
End Function

' Reduce Meetings by TimeAdjustment minutes if duration is multiple of 30 minutes
Function MeetingAdjustDuration(MeetingDuration As Integer, Optional TimeAdjustment As Integer = 10) As Integer
  LoggerIndent "MeetingAdjustDuration"

  Dim NewDuration As Integer

  If MeetingDuration Mod 30 = 0 Then
    NewDuration = MeetingDuration - TimeAdjustment
  Else
    NewDuration = MeetingDuration
  End If

  LoggerDebugLib MeetingDuration & " -> " & NewDuration
  MeetingAdjustDuration = NewDuration

  LoggerDedent
End Function

Function IsMeetingCanceled(OutlookItem As Object) As Boolean
  LoggerIndent "IsMeetingCanceled"

  If OutlookItem.MessageClass = "IPM.Schedule.Meeting.Canceled" Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsMeetingCanceled = True
  End If

  LoggerDedent
End Function

Function IsMeetingProposeNewTime(OutlookItem As Object) As Boolean
  LoggerIndent "IsMeetingProposeNewTime"

  If StartsWith(OutlookItem.MessageClass, "IPM.Schedule.Meeting.Resp.") _
  And StartsWith(OutlookItem.Subject, "New Time Proposed: ") _
  Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsMeetingProposeNewTime = True
  End If

  LoggerDedent
End Function

Function IsMeetingRequest(OutlookItem As Object) As Boolean
  LoggerIndent "IsMeetingRequest"

  If OutlookItem.MessageClass = "IPM.Schedule.Meeting.Request" Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsMeetingRequest = True
  End If

  LoggerDedent
End Function

Function IsMeetingResponseAccept(OutlookItem As Object) As Boolean
  LoggerIndent "IsMeetingResponseAccept"

  If OutlookItem.MessageClass = "IPM.Schedule.Meeting.Resp.Pos" Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsMeetingResponseAccept = True
  End If

  LoggerDedent
End Function

Function IsMeetingResponseDecline(OutlookItem As Object) As Boolean
  LoggerIndent "IsMeetingResponseDecline"

  If OutlookItem.MessageClass = "IPM.Schedule.Meeting.Resp.Neg" _
  And StartsWith(OutlookItem.Subject, "Declined: ") _
  Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsMeetingResponseDecline = True
  End If

  LoggerDedent
End Function

Function IsMeetingResponseTentative(OutlookItem As Object) As Boolean
  LoggerIndent "IsMeetingResponseAccept"

  If OutlookItem.MessageClass = "IPM.Schedule.Meeting.Resp.Tent" Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsMeetingResponseTentative = True
  End If

  LoggerDedent
End Function

