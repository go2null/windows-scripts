Attribute VB_Name = "FolderReceipt"
Option Base 0
Option Compare Text
Option Explicit

Private Const ParentFolderName As String = "Sent Items"

' | Item                              | TX/RX | Unread | Actions     | MessageClass
' | --------------------------------- | ----- | ------ | ----------- | ------------
' | Automatic Replies                 | *     | *      | Move        | IPM.Note.Rules.ReplyTemplate.Microsoft
' | OOO Response                      | *     | Unread | -           | IPM.Note.Rules.OofTemplate.Microsoft
' | OOO Response                      | *     | Read   | Move        | IPM.Note.Rules.OofTemplate.Microsoft
' | Meeting Request                   | *     | Unread | -           | IPM.Schedule.Meeting.Request
' | Meeting Request                   | *     | Read   | Move        | IPM.Schedule.Meeting.Request
' | Meeting Response Accept           | *     | *      | Read + Move | IPM.Schedule.Meeting.Resp.Pos
' | Meeting Response Tentative        | *     | *      | Read + Move | IPM.Schedule.Meeting.Resp.Tent
' | Meeting Response Decline          | Rx    | Unread | -           | IPM.Schedule.Meeting.Resp.Neg
' | Meeting Response Decline          | Rx    | Read   | Move        | IPM.Schedule.Meeting.Resp.Neg
' | Meeting Response Decline          | Sent  | *      | Move        | IPM.Schedule.Meeting.Resp.Neg
' | Meeting Response Propose New Time | Rx    | Unread | -           | IPM.Schedule.Meeting.Resp.
' | Meeting Response Propose New Time | Rx    | Read   | Move        | IPM.Schedule.Meeting.Resp.
' | Meeting Response Propose New Time | Sent  | *      | Move        | IPM.Schedule.Meeting.Resp.
' | Meeting Cancellation              | *     | Unread | -           | IPM.Schedule.Meeting.Canceled
' | Meeting Cancellation              | *     | Read   | Move        | IPM.Schedule.Meeting.Canceled
' | Delivery Report                   | *     | *      | Read + Move | REPORT.*.DR
' | Undeliverable Report              | *     | Unread | -           | REPORT.*.NDR
' | Undeliverable Report              | *     | Read   | Move        | REPORT.*.NDR
' | Read Receipt                      | *     | *      | Read + Move | REPORT.*.IPNRN
' | Deleted Not Read Report           | *     | *      | Move        | REPORT.*.IPNNRN
' | Recall Message                    | *     | *      | Move        | IPM.Outlook.Recall?
' | Recall Failure Report             | *     | *      | Move        | IPM.Recall.Report.Failure
' | Recall Success Report             | *     | *      | Read + Move | IPM.Recall.Report.Success?
'
' IPM - InterPersonal Messager
' IPN - InterPersonal Note


Function Receipts_MoveToReceiptsFolder(FolderItem As Object, ReceiptsFolderName As String) As Boolean
  LoggerIndent "FolderReceipts_Move"

  Static ReceiptsFolder As Outlook.Folder

  'skip signed messages as they do not allow access to attributes and methods
  If Not IsAutomatableItem(FolderItem) Then GoTo ExitProcedure

  If ReceiptsFolder Is Nothing Then
    Set ReceiptsFolder = FindFolder(ReceiptsFolderName, GetPrimaryMailbox.GetRootFolder)
  End If

  If FolderItem.Parent.Name = ReceiptsFolder.Name Then GoTo ExitProcedure

  'If IsAlwaysArchiveReceipt(FolderItem) Then
  If IsAReadAndMoveItem(FolderItem) Then
    FolderItem.Unread = False
  'ElseIf IsReadyForArchiveReceipt(FolderItem) Then
  ElseIf IsAMoveOnlyItem(FolderItem) Then
    ' do nothing
  Else
    GoTo ExitProcedure
  End If

  Receipts_MoveToReceiptsFolder = MoveReceipt(FolderItem, ReceiptsFolder)

ExitProcedure:
  LoggerDedent
End Function

Private Function IsAReadAndMoveItem(FolderItem As Object) As Boolean
  LoggerIndent "IsAReadAndMoveItem"

  If IsMeetingResponseAccept(FolderItem) Then GoTo IsMatch
  If IsMeetingResponseTentative(FolderItem) Then GoTo IsMatch
  If IsMessageDeliveryReport(FolderItem) Then GoTo IsMatch
  If IsMessageReadReport(FolderItem) Then GoTo IsMatch
  If IsRecallSuccessReport(FolderItem) Then GoTo IsMatch

  GoTo ExitProcedure

IsMatch:
  IsAReadAndMoveItem = True

ExitProcedure:
  LoggerDedent
End Function

Private Function IsAMoveOnlyItem(FolderItem As Object) As Boolean
  LoggerIndent "IsAMoveOnlyItem"

  If IsAutomaticResponse(FolderItem) Then GoTo IsMatch
  If IsMessageDeletedNotReadReport(FolderItem) Then GoTo IsMatch

  If Not FolderItem.Unread Then
    If IsOOOResponse(FolderItem) Then GoTo IsMatch
    If IsMeetingRequest(FolderItem) Then GoTo IsMatch
    If IsMeetingResponseDecline(FolderItem) Then GoTo IsMatch
    If IsMeetingProposeNewTime(FolderItem) Then GoTo IsMatch
    If IsMeetingCanceled(FolderItem) Then GoTo IsMatch
    If IsMessageUndeliverableReport(FolderItem) Then GoTo IsMatch
    If IsRecallRequest(FolderItem) Then GoTo IsMatch
    If IsRecallFailureReport(FolderItem) Then GoTo IsMatch
  End If

  GoTo ExitProcedure

IsMatch:
  IsAMoveOnlyItem = True

ExitProcedure:
  LoggerDedent
End Function

Private Function MoveReceipt(FolderItem As Object, ReceiptsFolder As Outlook.Folder) As Boolean
  LoggerIndent "MoveReceipt"

  LoggerInfo FolderItem.Parent.FolderPath _
    & " -> " _
    & ReceiptsFolder.FolderPath _
    & " | " _
    & FolderItem.MessageClass
  FolderItem.Move ReceiptsFolder
  MoveReceipt = True

ExitProcedure:
  LoggerDedent
End Function

Private Sub PrintSelectionMessageClasses()
  Dim OutlookItem As Object

  For Each OutlookItem In Application.ActiveExplorer.Selection
    PrintMessageClass OutlookItem
  Next
End Sub

Private Sub PrintFolderMessageClasses()
  Dim OutlookItem As Object

  For Each OutlookItem In Application.ActiveExplorer.CurrentFolder.Items
    PrintMessageClass OutlookItem
  Next
End Sub

Private Sub PrintMessageClass(OutlookItem As Object)
  Static MessageClasses
  Const MaxLength As Byte = 40

  If Not ArrayIncludes(MessageClasses, OutlookItem.MessageClass) Then
    ArrayPush MessageClasses, OutlookItem.MessageClass

    Debug.Print StringRepeat("=", 80)

    Debug.Print _
      Format(Left(OutlookItem.Subject, MaxLength), "!" & StringRepeat("@", MaxLength)); _
      " | "; _
      OutlookItem.Unread; _
      " | "; _
      OutlookItem.MessageClass

    If Not IsSignedItem(OutlookItem) Then
      Debug.Print Left(OutlookItem.Body, 100)
    End If
  End If
End Sub

