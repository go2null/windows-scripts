Attribute VB_Name = "MeetingDuration"
Option Base 0
Option Compare Text
Option Explicit

Sub Trim5Minutes()
  LoggerIndent "Trim5Minutes"

  Dim CurrentItem As Object

  Set CurrentItem = Application.ActiveInspector.CurrentItem

  If CurrentItem.Class = olAppointment Then
    With CurrentItem
      .Start = MeetingAdjustTime(.Start, 5)
      .Duration = MeetingAdjustDuration(.Duration)
      .End = MeetingAdjustTime(.End, -5)
    End With
  End If

  LoggerDedent
End Sub
