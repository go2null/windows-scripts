Attribute VB_Name = "LibOutlookItem"
Option Base 0
Option Compare Text
Option Explicit

Function IsAutomaticResponse(OutlookItem As Object) As Boolean
  LoggerIndent "IsAutomaticResponse"

  If OutlookItem.MessageClass = "IPM.Note.Rules.ReplyTemplate.Microsoft" Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsAutomaticResponse = True
  End If

  LoggerDedent
End Function

Function IsMessageDeletedNotReadReport(OutlookItem As Object) As Boolean
  LoggerIndent "IsMessageDeletedNotReadReport"

  If StartsWith(OutlookItem.MessageClass, "REPORT.") _
  And EndsWith(OutlookItem.MessageClass, ".IPNNRN") _
  Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsMessageDeletedNotReadReport = True
  End If

  LoggerDedent
End Function

Function IsMessageDeliveryReport(OutlookItem As Object) As Boolean
  LoggerIndent "IsMessageDeliveryReport"

  If StartsWith(OutlookItem.MessageClass, "REPORT.") _
  And EndsWith(OutlookItem.MessageClass, ".DR") _
  Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsMessageDeliveryReport = True
  End If

  LoggerDedent
End Function

Function IsMessageReadReport(OutlookItem As Object) As Boolean
  LoggerIndent "IsMessageReadReport"

  If StartsWith(OutlookItem.MessageClass, "REPORT.") _
  And EndsWith(OutlookItem.MessageClass, ".IPNRN") _
  Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsMessageReadReport = True
  End If

  LoggerDedent
End Function

Function IsMessageUndeliverableReport(OutlookItem As Object) As Boolean
  LoggerIndent "IsMessageUndeliverableReport"

  If StartsWith(OutlookItem.MessageClass, "REPORT.") _
  And EndsWith(OutlookItem.MessageClass, ".NDR") _
  Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsMessageUndeliverableReport = True
  End If

  LoggerDedent
End Function

Function IsOOOResponse(OutlookItem As Object) As Boolean
  LoggerIndent "IsOOOResponse"

  If OutlookItem.MessageClass = "IPM.Note.Rules.OofTemplate.Microsoft" Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsOOOResponse = True
  End If

  LoggerDedent
End Function

Function IsRecallRequest(OutlookItem As Object) As Boolean
  LoggerIndent "IsRecallRequest"

  If OutlookItem.MessageClass = "IPM.Outlook.Recall" Then
Debug.Assert False
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsRecallRequest = True
  End If

  LoggerDedent
End Function

Function IsRecallFailureReport(OutlookItem As Object) As Boolean
  LoggerIndent "IsRecallFailureReport"

  If OutlookItem.MessageClass = "IPM.Recall.Report.Failure" Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsRecallFailureReport = True
  End If

  LoggerDedent
End Function

Function IsRecallSuccessReport(OutlookItem As Object) As Boolean
  LoggerIndent "IsRecallSuccessReport"

  If OutlookItem.MessageClass = "IPM.Recall.Report.Success" Then
    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
    IsRecallSuccessReport = True
  End If

  LoggerDedent
End Function

' Message Recall Item do not allow access via code
' Err.Number      = 430
' Err.Description = Class does not support Automation or does not support expected interface
'
' Message Recall Item only expose the following properties
' * Class           = olMail
' * DownloadState   = olFullItem
' * IsConflict      = False
' * MarkForDownload = olUnmarked
' * MessageClass    = IPM.Outlook.Recall
' * Permission      = olUnrestricted
' * Sent            = True
' * Subject         = bla bla bla
' * Submitted       = False
' * UnRead          = True
Function IsMessageRecallItem(OutlookItem As Object) As Boolean
  LoggerIndent "IsMessageRecallItem"

  If OutlookItem.MessageClass <> "IPM.Outlook.Recall" _
  Then GoTo ExitProcedure

    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
  IsMessageRecallItem = True

ExitProcedure:
  LoggerDedent
End Function

' Signed emails do not allow access via code
' Err.Number      = -2147217663
' Err.Description = This digitally signed e-mail has a receipt request and so cannot be opened in a UI-less mode.
'
' Signed MailItems only expose the following properties
' * Class           = olMail
' * DownloadState   = olFullItem
' * IsConflict      = False
' * MarkForDownload = olUnmarked
' * MessageClass    = IPM.Note.SMIME.MultipartSigned
' * Permission      = olUnrestricted
' * Sent            = True
' * Subject         = bla bla bla
' * Submitted       = False
' * UnRead          = True
Function IsSignedItem(OutlookItem As Object) As Boolean
  LoggerIndent "IsSignedItem"

  If OutlookItem.MessageClass <> "IPM.Note.SMIME.MultipartSigned" _
  Then GoTo ExitProcedure

    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
  IsSignedItem = True

ExitProcedure:
  LoggerDedent
End Function

Function IsAutomatableItem(OutlookItem As Object) As Boolean
  LoggerIndent "IsAutomatableItem"

  If IsMessageRecallItem(OutlookItem) Then GoTo ExitProcedure
  If IsSignedItem(OutlookItem) Then GoTo ExitProcedure

    LoggerDebugLib "TRUE " & OutlookItem.MessageClass
  IsAutomatableItem = True

ExitProcedure:
  LoggerDedent
End Function
