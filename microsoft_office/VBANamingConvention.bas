Attribute VB_Name = "VBANamingConvention"
' Rules, in increasing specificity

' Adhere to the Visual Basic Naming Conventions
'   https://learn.microsoft.com/en-us/dotnet/visual-basic/programming-guide/program-structure/naming-conventions

'   UpperCamelCase: Begin each separate word in a name with a capital letter,
'     as in FindLastRecord and RedrawMyForm.

'   Verbs: Begin function and method names with a verb,
'     as in InitNameArray or CloseDialog.

'   Nouns: Begin class, structure, module, and property names with a noun,
'     as in EmployeeName or CarAccessory.

'   Interfaces: Begin interface names with the prefix "I", followed by a noun or a noun phrase,
'     like IComponent, or with an adjective describing the interface's behavior, like IPersistable.
'     Do not use the underscore, and use abbreviations sparingly, because abbreviations can cause confusion.

'   Event Handlers: Begin event handler names with a noun describing the type of event followed by the "EventHandler" suffix,
'     as in "MouseEventHandler".

'   Event Argument Classes: In names of event argument classes, include the "EventArgs" suffix.

'   Before and After: If an event has a concept of "before" or "after," use a suffix in present or past tense,
'     as in "ControlAdd" or "ControlAdded".

'   Common Abbreviations: For long or frequently used terms, use abbreviations to keep name lengths reasonable,
'     for example, "HTML", instead of "Hypertext Markup Language".
'     In general, variable names greater than 32 characters are difficult to read on a monitor set to a low resolution.
'     Also, make sure your abbreviations are consistent throughout the entire application.
'     Randomly switching in a project between "HTML" and "Hypertext Markup Language" can lead to confusion.

'   Scope: Avoid using names in an inner scope that are the same as names in an outer scope.
'     Errors can result if the wrong variable is accessed.
'     If a conflict occurs between a variable and the keyword of the same name,
'       you must identify the keyword by preceding it with the appropriate type library.
'     For example, if you have a variable called Date,
'       you can use the intrinsic Date function only by calling DateTime.Date.

' Adhere to Visual Basic (for Applications) Naming Rules
'   https://learn.microsoft.com/en-us/office/vba/language/concepts/getting-started/visual-basic-naming-rules
'   Prevent shadowing (VBA or host application) built-ins
'     Append `_` where there might be shadowing
'
' MINE
'   Get prefix - function that returns the object or value
'   Set prefix - function that sets the object or value and returns TRUE or FALSE

Option Base 0
Option Compare Text
Option Explicit

Private Function IsSomething() As Boolean
  LoggerIndent "IsSomething"

  Dim ErrorNumber As Long, ErrorDescription As String

  On Error GoTo ErrorHandler

  Dim ReturnValue As Boolean

  LoggerDebugLib ReturnValue
  IsSomething = ReturnValue

  GoTo ExitProcedure

ErrorHandler:
  ErrorNumber = Err.Number
  ErrorDescription = Err.Description
  If MatchError(ErrorNumber, ErrorDescription, MatchDescription:="Start of, or full, Error Description") Then
    Resume Next
  Else
    Err.Raise ErrorNumber
  End If

ExitProcedure:
  LoggerDedent
End Function
