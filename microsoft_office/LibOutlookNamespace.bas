Attribute VB_Name = "LibOutlookNamespace"
Option Base 0
Option Compare Text
Option Explicit

Function IsFastConnection() As Boolean
  LoggerIndent "IsFastConnection"

  Dim MAPINameSpace As Outlook.NameSpace
  Dim ConnectionMode As Long

  Set MAPINameSpace = Application.GetNamespace("MAPI")
  ConnectionMode = MAPINameSpace.ExchangeConnectionMode

  If ConnectionMode = olCachedConnectedFull _
    Or ConnectionMode = olOnline _
  Then
    IsFastConnection = True
  End If

  LoggerDedent
End Function
