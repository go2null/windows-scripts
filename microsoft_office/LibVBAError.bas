Attribute VB_Name = "LibVBAError"
Option Base 0
Option Compare Text
Option Explicit

Function MatchError(Optional ErrorNumber As Long, Optional ErrorDescription As String, Optional MatchNumber As Long, Optional MatchDescription As String) As Boolean
  LoggerIndent "MatchError"

  Dim ErrorMatched As Boolean

  If MatchNumber <> 0 And ErrorNumber = MatchNumber Then
    GoTo ErrorMatched
  End If

  If MatchDescription <> "" And StartsWith(ErrorDescription, MatchDescription) Then
    GoTo ErrorMatched
  End If

  LoggerError ErrorNumber & " " & ErrorDescription

  GoTo ExitProcedure

ErrorMatched:
  MatchError = True

ExitProcedure:
  LoggerDedent
End Function
