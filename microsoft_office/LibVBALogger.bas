Attribute VB_Name = "LibVBALogger"
Option Base 0
Option Compare Text
Option Explicit

Private LogProcedures() As String
Private LogIndentLevel As Byte

Private Const logDebugLib As Byte = 8 ' Library functions
Private Const logDebug As Byte = 7    ' SysLog debug
Private Const logInfo As Byte = 6     ' Syslog info
Private Const logWarn As Byte = 4     ' Syslog warning
Private Const logError As Byte = 3    ' Syslog err
Private Const logFatal As Byte = 1    ' Syslog alert
Private Const logUnknown As Byte = 0  ' Syslog emerg

Private Const MaxLogLevel As Byte = logInfo
'Private Const MaxLogLevel As Byte = logDebugLib

Sub LoggerIndent(Optional ProcedureName As String)
  Dim NewIndex As Long

  If ProcedureName <> "" Then
    ' Cannot use ArrayPush as it leads to infinite recursion
    NewIndex = LogProceduresSize + 1
    ReDim Preserve LogProcedures(NewIndex)
    LogProcedures(NewIndex) = ProcedureName

    ' Has to be after LogProcedures is initialized
    LoggerDebugLib "ENTER"
  End If

  LogIndentLevel = LogIndentLevel + 1
End Sub

Sub LoggerDedent(Optional PopStack As Boolean = True)
  LogIndentLevel = LogIndentLevel - 1

  Dim ProceduresSize As Long

  ProceduresSize = LogProceduresSize

  If LogIndentLevel >= ProceduresSize Then GoTo ExitProcedure

  If PopStack Then
    ' Cannot use ArrayPop as it leads to infinite recursion
    ReDim Preserve LogProcedures(ProceduresSize - 1)

    LoggerDebugLib "EXIT"
  End If

ExitProcedure:
End Sub

Sub Logger(LogLevel As Byte, LogMessage)
  Static LogLevels(logDebugLib) As String
  Dim LogMessageText As String, TimeStamp As String, ProcName As String

  If LogLevels(logDebugLib) = "" Then
    LogLevels(logDebugLib) = "DEBUGLIB "
    LogLevels(logDebug) = "DEBUG    "
    LogLevels(logInfo) = "INFO     "
    LogLevels(logWarn) = "WARN     "
    LogLevels(logError) = "ERROR    "
    LogLevels(logFatal) = "FATAL    "
    LogLevels(logUnknown) = "UNKNOWN  "
  End If

  On Error Resume Next
    LogMessageText = CStr(LogMessage)
  On Error GoTo 0

  If LogLevel > MaxLogLevel Then GoTo ExitProcedure

  TimeStamp = Format(Time, "hh:mm:ss." & Right(Format(Timer, "#0.00"), 2))
  Debug.Print TimeStamp; Spc(1); LogLevels(LogLevel); Spc(1); LogIndentLevel; Spc(LogIndentLevel * 2); LogProceduresCurrent, LogMessageText

ExitProcedure:
End Sub

Sub LoggerDebugLib(LogMessage)
  Logger logDebugLib, LogMessage
End Sub

Sub LoggerDebug(LogMessage)
  Logger logDebug, LogMessage
End Sub

Sub LoggerError(LogMessage)
  Logger logError, LogMessage
End Sub

Sub LoggerFatal(LogMessage)
  Logger logFatal, LogMessage
End Sub

Sub LoggerInfo(LogMessage)
  Logger logInfo, LogMessage
End Sub

Sub LoggerUnknown(LogMessage)
  Logger logUnknown, LogMessage
End Sub

Sub LoggerWarn(LogMessage)
  Logger logWarn, LogMessage
End Sub

Private Function LogProceduresSize() As Long
  Dim CurrentIndex As Long

  On Error Resume Next
    CurrentIndex = UBound(LogProcedures)
  On Error GoTo 0

  LogProceduresSize = CurrentIndex
End Function

Private Function LogProceduresCurrent() As String
  LogProceduresCurrent = LogProcedures(LogProceduresSize)
End Function
