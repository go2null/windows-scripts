Attribute VB_Name = "StoreOfflineArchive"
Option Base 0
Option Compare Text
Option Explicit

' Start of USER Config
Private Const ARCHIVE_NAME_SUFFIX As String = "-Archive"
Private Const DEFAULT_ARCHIVE_NAME As String = "Bell-Archive"
' End of USER Config

Private Const APP_NAME As String = "Move to Offline Archives"
Private OfflineArchives()

Sub MoveToOfflineArchive()
  LoggerIndent "MoveToOfflineArchive"

  Dim OnlineArchive As Outlook.Store
  Dim DefaultOfflineParentFolder As Outlook.Folder
  Dim OnlineMainFolder As Outlook.Folder, OfflineMainFolder As Outlook.Folder

  ' only run if connected to Exchange on a fast connection
  If Not IsFastConnection Then GoTo ExitProcedure

  Set OnlineArchive = GetOnlineArchive
  If OnlineArchive Is Nothing Then GoTo ExitProcedure

  If Not SetOfflineArchives Then GoTo ExitProcedure

  Set DefaultOfflineParentFolder = GetDefaultOfflineParentFolder
  If DefaultOfflineParentFolder Is Nothing Then GoTo ExitProcedure

  For Each OnlineMainFolder In OnlineArchive.GetRootFolder.Folders
    Set OfflineMainFolder = GetOfflineMainFolder(OnlineMainFolder.Name)

    If OfflineMainFolder Is Nothing Then
      OnlineMainFolder.MoveTo DefaultOfflineParentFolder
    Else
      MoveFolder OnlineMainFolder, OfflineMainFolder.Parent
    End If
  Next

ExitProcedure:
  LoggerDedent True
End Sub

Private Function GetOnlineArchive() As Outlook.Store
  LoggerIndent "GetOnlineArchive"
  Dim ErrorNumber As Long
  On Error GoTo ErrorHandler

  Dim PrimaryMailboxName As String, OnlineArchiveName As String
  Dim OutlookStore As Outlook.Store, OnlineArchive As Outlook.Store
  Dim LogMessage As String

  PrimaryMailboxName = GetPrimaryMailboxName
  If PrimaryMailboxName = "" Then GoTo CheckValue

  OnlineArchiveName = "Online Archive - " & PrimaryMailboxName

  For Each OutlookStore In Application.Session.Stores
    With OutlookStore
      If .ExchangeStoreType = olExchangeMailbox _
      And .DisplayName = OnlineArchiveName _
      Then
        Set OnlineArchive = OutlookStore
        Exit For
      End If
    End With
  Next

CheckValue:
  If OnlineArchive Is Nothing Then
    LogMessage = "Cannot find '" & OnlineArchiveName & "'."
    LoggerError LogMessage
    MsgBox LogMessage, vbCritical, APP_NAME
  Else
    Set GetOnlineArchive = OnlineArchive
  End If

  GoTo ExitProcedure

ErrorHandler:
  ErrorNumber = Err.Number
  If MatchError(ErrorDescription:=Err.Description, MatchDescription:="Automation error") Then
    Resume Next
  Else
    Err.Raise ErrorNumber
  End If

ExitProcedure:
  LoggerDedent True
End Function

Private Function SetOfflineArchives() As Boolean
  LoggerIndent "SetOfflineArchives"

  Dim OutlookStore As Outlook.Store
  Dim LogMessage As String

  For Each OutlookStore In Application.Session.Stores
    With OutlookStore
      If .ExchangeStoreType = olNotExchange _
      And Right(.DisplayName, 8) = ARCHIVE_NAME_SUFFIX Then
        ArrayPush OfflineArchives(), OutlookStore
      End If
    End With
  Next

  If ArrayIsEmpty(OfflineArchives) Then
    LogMessage = "Cannot find any offline archive ('*" & ARCHIVE_NAME_SUFFIX & "')."
    LoggerError LogMessage
    MsgBox LogMessage, vbCritical, APP_NAME
  Else
    SetOfflineArchives = True
  End If

  LoggerDedent True
End Function

Private Function GetDefaultOfflineParentFolder() As Outlook.Folder
  LoggerIndent "GetDefaultOfflineParentFolder"

  Dim ArchiveStore As Outlook.Store, i As Long
  Dim LogMessage As String

  For i = LBound(OfflineArchives) To UBound(OfflineArchives)
    If OfflineArchives(i).DisplayName = DEFAULT_ARCHIVE_NAME Then
      Set ArchiveStore = OfflineArchives(i)
      Exit For
    End If
  Next i

  If ArchiveStore Is Nothing Then
    LogMessage = "Cannot find '" & DEFAULT_ARCHIVE_NAME & "'."
    LoggerFatal LogMessage
    MsgBox LogMessage, vbCritical, APP_NAME
  Else
    Set GetDefaultOfflineParentFolder = ArchiveStore.GetRootFolder
  End If

  LoggerDedent True
End Function

' RETURN first main (1st-level) folder in OfflineArchives that matches FolderName
Private Function GetOfflineMainFolder(FolderName As String) As Outlook.Folder
  LoggerIndent "GetOfflineMainFolder"

  Dim AlnumName As String
  Dim i As Long, MainFolder As Outlook.Folder
  Dim OfflineMainFolder As Outlook.Folder

  AlnumName = AlphaNumerics(FolderName)

  For i = LBound(OfflineArchives) To UBound(OfflineArchives)
    For Each MainFolder In OfflineArchives(i).GetRootFolder.Folders
      If AlphaNumerics(MainFolder.Name) = AlnumName Then
        Set OfflineMainFolder = MainFolder
        Exit For
      End If
    Next
  Next i

  If Not OfflineMainFolder Is Nothing Then Set GetOfflineMainFolder = OfflineMainFolder

  LoggerDedent True
End Function
