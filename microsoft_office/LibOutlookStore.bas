Attribute VB_Name = "LibOutlookStore"
Option Base 0
Option Compare Text
Option Explicit

Dim DefaultFolders()
Dim DefaultFolderNames()

Function GetDefaultFolders()
  LoggerIndent "GetDefaultFolders"

  If ArrayIsEmpty(DefaultFolders) Then InitDefaultFolders

  GetDefaultFolders = DefaultFolders

  LoggerDebugLib UBound(DefaultFolders) + 1 & " default folders"
  LoggerDedent
End Function

Private Sub InitDefaultFolders()
  LoggerIndent "InitDefaultFolders"

  Dim FolderTypes, FolderType
  Dim OutlookStore As Outlook.Store, OutlookFolder As Outlook.Folder

  FolderTypes = Array(olFolderInbox, olFolderSentMail, _
                      olFolderCalendar, olFolderConflicts, _
                      olFolderDeletedItems, olFolderDrafts, _
                      olFolderJournal, olFolderJunk, _
                      olFolderLocalFailures, olFolderManagedEmail, _
                      olFolderNotes, olFolderOutbox, _
                      olFolderRssFeeds, olFolderServerFailures, _
                      olFolderSuggestedContacts, olFolderSyncIssues, _
                      olFolderTasks, olPublicFoldersAllPublicFolders)

  For Each OutlookStore In Application.GetNamespace("MAPI").Stores
    If Not IsUserStore(OutlookStore) Then GoTo NextStore

    For Each FolderType In FolderTypes
      Set OutlookFolder = Nothing
      On Error Resume Next
        Set OutlookFolder = OutlookStore.GetDefaultFolder(FolderType)
      On Error GoTo 0

      If OutlookFolder Is Nothing Then GoTo NextFolderType
      If OutlookFolder.Name = "" Then GoTo NextFolderType

      LoggerDebugLib "Folder " & OutlookFolder.Name & " " & OutlookFolder.FolderPath
      ArrayPush DefaultFolders, OutlookFolder

NextFolderType:
    Next

NextStore:
  Next

  LoggerDebugLib UBound(DefaultFolders) + 1 & " default folders"
  LoggerDedent
End Sub

Function GetDefaultFolderNames()
  LoggerIndent "GetDefaultFolderNames"

  If ArrayIsEmpty(DefaultFolderNames) Then InitDefaultFolderNames

  GetDefaultFolderNames = DefaultFolderNames

  LoggerDedent
End Function

Private Sub InitDefaultFolderNames()
  LoggerIndent "InitDefaultFolderNames"

  Dim TheDefaultFolders, TheIndex As Long, TheName As String

  DefaultFolderNames = Array( _
    "Conversation Action Settings", _
    "Conversation History", _
    "Files", _
    "GAL Contacts", _
    "Inbound", _
    "Organizational Contacts", _
    "Outbound", _
    "PeopleCentricConversation Buddies", _
    "Working Set" _
  )

  TheDefaultFolders = GetDefaultFolders

  For TheIndex = LBound(TheDefaultFolders) To UBound(TheDefaultFolders)
    TheName = TheDefaultFolders(TheIndex).Name

    If Not ArrayIncludes(DefaultFolderNames, TheName) Then
      ArrayPush DefaultFolderNames, TheName
    End If
  Next

  LoggerDebugLib UBound(TheDefaultFolders) + 1 & " default folder names"
  LoggerDedent
End Sub

Function GetPrimaryInbox() As Outlook.Folder
  LoggerIndent "GetPrimaryInbox"

  Static PrimaryInbox As Outlook.Folder

  If PrimaryInbox Is Nothing Then
    Set PrimaryInbox = GetPrimaryMailbox.GetDefaultFolder(olFolderInbox)
  End If

  If Not PrimaryInbox Is Nothing Then Set GetPrimaryInbox = PrimaryInbox

  Debug.Print PrimaryInbox

  LoggerDedent
End Function

Function GetPrimarySentItems() As Outlook.Folder
  LoggerIndent "GetPrimaryInbox"

  Static PrimarySentItems As Outlook.Folder

  If PrimarySentItems Is Nothing Then
    Set PrimarySentItems = GetPrimaryMailbox.GetDefaultFolder(olFolderSentMail)
  End If

  If Not PrimarySentItems Is Nothing Then Set GetPrimarySentItems = PrimarySentItems

  Debug.Print PrimarySentItems

  LoggerDedent
End Function

Function GetPrimaryMailbox() As Outlook.Store
  LoggerIndent "GetPrimaryMailbox"

  Static PrimaryMailbox As Outlook.Store

  If PrimaryMailbox Is Nothing Then
    Set PrimaryMailbox = Outlook.Application.Session.DefaultStore
  End If

  If Not PrimaryMailbox Is Nothing Then
    Set GetPrimaryMailbox = PrimaryMailbox
  End If

  LoggerDebugLib PrimaryMailbox.DisplayName
  LoggerDedent
End Function

Function GetPrimaryMailboxName() As String
  LoggerIndent "GetPrimaryMailboxName"

  Static PrimaryMailboxName As String

  If PrimaryMailboxName = "" Then
    PrimaryMailboxName = GetPrimaryMailbox.DisplayName
  End If

  GetPrimaryMailboxName = PrimaryMailboxName

  LoggerDebugLib PrimaryMailboxName
  LoggerDedent
End Function

Function GetPrimaryMailboxRules()
  LoggerIndent "GetPrimaryMailboxRules"

  Dim ValidRules() As Outlook.Rule, Index As Long

  Dim PrimaryMailbox As Outlook.Store
  Dim MailboxRules As Outlook.Rules, Cursor As Long, MailboxRule As Outlook.Rule

  Set PrimaryMailbox = GetPrimaryMailbox
  Set MailboxRules = PrimaryMailbox.GetRules

  ' For Each only returns the first 5 rules in Microsoft 365
  For Cursor = 1 To MailboxRules.Count
    Set MailboxRule = Nothing

    ' Some rules cannot be retrieved :(
    On Error Resume Next
      Set MailboxRule = MailboxRules(Cursor)
    On Error GoTo 0

    If Not MailboxRule Is Nothing Then
      Index = Index + 1
      ReDim Preserve ValidRules(Index)
      Set ValidRules(Index) = MailboxRule
    End If
  Next

  GetPrimaryMailboxRules = ValidRules()

  LoggerDebugLib UBound(ValidRules) + 1 & " rules"
  LoggerDedent
End Function

Function CreateMailboxRule(MailboxRules As Outlook.Rules, RuleName As String, Optional RuleType As OlRuleType = olRuleReceive) As Outlook.Rule
  LoggerIndent "GetPrimaryMailboxRule"

  Dim ErrorNumber As Long, ErrorDescription As String
  On Error GoTo ErrorHandler

  Dim MailboxRule As Outlook.Rule

  Set MailboxRule = MailboxRules(RuleName)
  If Not MailboxRule Is Nothing Then
    MailboxRules.Remove (RuleName)
    MailboxRules.Save
  End If
  Set MailboxRule = MailboxRules.Create(RuleName, RuleType)

  Set CreateMailboxRule = MailboxRule

  GoTo ExitProcedure

ErrorHandler:
  ErrorNumber = Err.Number
  ErrorDescription = Err.Description
  If MatchError(ErrorNumber, ErrorDescription, MatchDescription:="The attempted operation failed.  An object could not be found.") Then
    Resume Next
  Else
    Err.Raise ErrorNumber
  End If

ExitProcedure:
  LoggerDedent
End Function

Function IsSentItemsFolder(OutlookFolder As Outlook.Folder) As Boolean
  LoggerIndent "IsSentItemsFolder"

  If OutlookFolder.Name = "Sent Items" Then IsSentItemsFolder = True

  LoggerDedent
End Function

Function IsInboxFolder(OutlookFolder As Outlook.Folder) As Boolean
  LoggerIndent "IsInboxFolder"

  If OutlookFolder.Name = "Inbox" Then IsInboxFolder = True

  LoggerDedent
End Function

Function IsDefaultFolder(OutlookFolder As Outlook.Folder) As Boolean
  LoggerIndent "IsDefaultFolder"

  Dim DefaultFolder

  For Each DefaultFolder In GetDefaultFolders
    If OutlookFolder = DefaultFolder Then
      IsDefaultFolder = True
      Exit For
    End If
  Next

  LoggerDedent
End Function

Function IsRootFolder(OutlookFolder As Outlook.Folder) As Boolean
  LoggerIndent "IsRootFolder"

  If OutlookFolder.Parent.Class = olNamespace Then IsRootFolder = True

  LoggerDedent
End Function

Function IsUserStore(OutlookStore As Outlook.Store) As Boolean
  LoggerIndent "IsUserStore"

  Dim IsAUserStore As Boolean

  If OutlookStore.FilePath <> "" Then
    IsAUserStore = True
  ElseIf OutlookStore.DisplayName = "Online Archive - " & GetPrimaryMailboxName Then
    IsAUserStore = True
  End If

  If IsAUserStore Then
    LoggerDebugLib OutlookStore.DisplayName & " is a User Store."
    IsUserStore = True
  Else
    LoggerDebugLib OutlookStore.DisplayName & " is NOT a User Store."
  End If

  LoggerDedent
End Function
